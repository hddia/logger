/*
   Copyright 2020 Bota Viorel

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

pub mod log
{
    extern crate chrono;
    use chrono::Utc;

    fn log_message(message_group: String, message: String)
    {
        println!("{} - {}: {}", Utc::now().to_rfc2822(), message_group, message)
    }

    pub fn error(message: String)
    {
        log_message(format!("ERROR"), message);
    }

    pub fn trace(message: String)
    {
        log_message(format!("TRACE"), message);
    }

    pub fn fatal(message: String)
    {
        log_message(format!("FATAL"), message);
    }

    #[cfg(debug_assertions)]
    pub fn debug(message: String) {
        log_message(format!("DEBUG"), message);
    }

}